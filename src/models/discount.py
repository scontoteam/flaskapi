from app import app,db


class Category(db.Model):
    __tablename__ = 'category'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))

    def __repr__(self):
        return self.name


class TimeOfDay(db.Model):
    __tablename__ = 'time'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))

    def __repr__(self):
        return self.name


class Overlay(db.Model):
    __tablename__ = 'overlay'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))

    def __repr__(self):
        return self.name


class Height(db.Model):
    __tablename__ = 'height'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))

    def __repr__(self):
        return self.name


class Discount(db.Model):
    __tablename__ = 'discount'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100))
    details = db.Column(db.String(1000))
    cafe = db.Column(db.String(100))
    location = db.Column(db.String(250))
    image = db.Column(db.String(1500))
    # foreign keys

    category_id = db.Column(db.Integer, db.ForeignKey('category.id', ondelete='CASCADE'))
    time_id = db.Column(db.Integer, db.ForeignKey('time.id', ondelete='CASCADE'))
    overlay_id = db.Column(db.Integer, db.ForeignKey('overlay.id', ondelete='CASCADE'))
    height_id = db.Column(db.Integer, db.ForeignKey('height.id', ondelete='CASCADE'))

    #relations
    category = db.relationship('Category')
    time = db.relationship('TimeOfDay')
    overlay = db.relationship('Overlay')
    height = db.relationship('Height')

    
    def __repr__(self):
        return self.title

    def __init__(self, title, details, cafe, location, image, category_id, time_id, overlay_id, height_id):
        self.title = title
        self.details = details
        self.cafe = cafe
        self.location = location
        self.category_id = category_id
        self.time_id = time_id
        self.overlay_id = overlay_id
        self.height_id = height_id
        self.image = image

    @classmethod
    def delete_all(cls):
        try:
            num_rows_deleted = db.session.query(cls).delete()
            print("DELETED ALL")
            db.session.commit()
            return {'message': '{} row(s) deleted'.format(num_rows_deleted)}
        except:
            return {'message': 'Something went wrong'}
    @classmethod
    def delete_by_index(cls, index):
        try:
            
            db.session.query(cls).filter_by(id=index).delete()
            db.session.commit()
            return {
                'message': 'deleted'
            }
        except:
            return {
                'message':'not deleted'
            }, 400