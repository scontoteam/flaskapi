from app import db
from passlib.hash import pbkdf2_sha256 as sha256
from flask_jwt import JWT
class User(db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key = True)
    email = db.Column(db.String(120), unique = True, nullable = False)
    password = db.Column(db.String(120), nullable = False)
    
    def save_to_db(self):
        db.session.add(self)
        db.session.commit()

    @classmethod
    def find_by_email(cls, email):
        return cls.query.filter_by(email = email).first()

    @classmethod
    def return_all(cls):
        def to_json(x):
            return {
                'id': x.id,
                'email': x.email,
            }
        return list(map(lambda x: to_json(x), User.query.all()))

    @classmethod
    def delete_all(cls):
        try:
            num_rows_deleted = db.session.query(cls).delete()
            db.session.commit()
            return {'message': '{} row(s) deleted'.format(num_rows_deleted)}
        except:
            return {'message': 'Something went wrong'}
            
    @classmethod
    def delete_by_index(cls, index):
        try:
            
            db.session.query(cls).filter_by(id=index).delete()
            db.session.commit()
            return {
                'message': 'deleted'
            }
        except:
            return {
                'message':'not deleted'
            }, 400
    
    @staticmethod
    def generate_hash(password):
        return sha256.hash(password)
    
    @staticmethod
    def verify_hash(password, hash):
        return sha256.verify(password, hash)

    @staticmethod
    def identity(payload):
        user_id = payload['identity']
        return {"id": user_id}

    def __repr__(self):
        return self.id





class UserLike(db.Model):
    # relations
    discount = db.relationship('Discount')
    owner = db.relationship('User')
    # columns
    id = db.Column(db.Integer, primary_key = True)
    discount_id = db.Column(db.Integer, db.ForeignKey('discount.id'))
    owner_id = db.Column(db.Integer,db.ForeignKey('users.id'))
    
    @classmethod
    def delete_by_index(cls, index):
        try:
            
            db.session.query(cls).filter_by(id=index).delete()
            db.session.commit()
            return {
                'message': 'deleted'
            }
        except:
            return {
                'message':'not deleted'
            }, 400
    
    @classmethod
    def return_likes(cls,user):
        print(user)
        likes = cls.query.filter_by(owner_id=user)
        
        result = []
        for like in likes:
            result.append({
                'id': like.id,
                'discount': {
                    'id': like.discount_id,
                    'title': like.discount.title,
                    'details': like.discount.details,
                    'cafe': like.discount.cafe,
                    'image': like.discount.image,
                    'location': like.discount.location,
                    'category': like.discount.category.name,
                    'time': like.discount.time.name,
                    'overlay': like.discount.overlay.name,
                    'height': like.discount.height.name
                    },
                    'owner': like.owner_id
            })
        return result
    def __init__(self,discount, owner):
        self.discount_id = int(discount)
        self.owner_id = owner
        
    def save_to_db(self):
        db.session.add(self)
        db.session.commit()