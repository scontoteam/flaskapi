from flask_restful import Resource, reqparse, request
from flask_jwt import current_identity
from models.user import User, UserLike
import jwt
from app import app
from flask_jwt_extended import (
    create_access_token, create_refresh_token, jwt_required,
    jwt_refresh_token_required, get_jwt_identity,
    get_raw_jwt)
parser = reqparse.RequestParser()
class UserLikeSingle(Resource):
    @jwt_required
    def delete(self, index):
        like = UserLike.query.filter_by(id=index).first()
        
        if like.owner_id == get_jwt_identity():
            print('start_delete')
            return UserLike.delete_by_index(index)
        else:
            return "forbidden", 403

class UserLikes(Resource):
    @jwt_required
    def get(self):
        current_user = get_jwt_identity()
        likes = UserLike.return_likes(current_user)
        return likes
    
    @jwt_required
    def post(self):
        parser.add_argument('discount', help = 'Discount cannot be blank', required = True)
        args = parser.parse_args()
        discount = args['discount']
        if discount:
            current_user = get_jwt_identity()
            old_like = UserLike.query.filter_by(discount_id=discount).filter_by(owner_id=get_jwt_identity()).first()
            if not old_like:
                like_new = UserLike(discount=discount,owner=current_user)
                like_new.save_to_db()
                return{
                    'id': like_new.id,
                    'discount': like_new.discount_id,
                    'owner': like_new.owner_id
                }
            else:
                return "you have already liked", 403

class GetIdentity(Resource):
    @jwt_required
    def get(self):
        tmp, auth_token = request.headers.get('Authorization').split(' ')
        payload = jwt.decode(auth_token, app.config['JWT_SECRET_KEY'], verify=True)
        
        return User.identity(payload)
