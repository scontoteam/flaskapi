from flask_restful import Resource, Api, reqparse
from flask import Flask, render_template, request, flash, url_for, \
    send_from_directory, Response, jsonify
from models.discount import Discount
from werkzeug.utils import secure_filename, redirect
import os
from app import app,db
BASE_DIR = app.config.get('BASE_DIR')
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])
UPLOAD_FOLDER = os.path.join(BASE_DIR,'uploads')

def allowed_file(filename):
    return '.' in filename and \
        filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


class DiscountSingleView(Resource):
    def delete(self,discount_index):
        return Discount.delete_by_index(discount_index)


class DiscountsView(Resource):
    def get(self):
        discounts = Discount.query.all()
        result = []
        for discount in discounts:
            result.append({
                'id': discount.id,
                'title': discount.title,
                'details': discount.details,
                'cafe': discount.cafe,
                'image': discount.image,
                'location': discount.location,
                'category': discount.category.name,
                'time': discount.time.name,
                'overlay': discount.overlay.name,
                'height': discount.height.name
            })
        return result

    def post(self):
        filename = None
        if 'image' not in request.files:
            flash('file not founded')
            return{
                "status": 'file not founded'
            }
        if 'image' in request.files:
            file = request.files['image']

            if file and allowed_file(file.filename):

                filename = secure_filename(file.filename)
                file.save(os.path.join(UPLOAD_FOLDER, filename))

        parser = reqparse.RequestParser()
        parser.add_argument('title', type=str)
        parser.add_argument('details', type=str)
        parser.add_argument('cafe', type=str)
        parser.add_argument('location', type=str)
        parser.add_argument('category', type=int)
        parser.add_argument('time', type=int)
        parser.add_argument('overlay', type=int)
        parser.add_argument('height', type=int)
        args = parser.parse_args()
        title = args['title']
        details = args['details']
        cafe = args['cafe']
        location = args['location']
        image = os.path.join("/uploads/", filename)
        category = args['category']
        time = args['time']
        overlay = args['overlay']
        height = args['height']

        print(title,details,cafe,location,image,category,time,overlay,height)
        if title and details and cafe and location and image and category and time and overlay and height:
            discount = Discount(title,details,cafe,location,image,category,time,overlay,height)
            db.session.add(discount)
            db.session.commit()
            return {
                'discount_status': 'created'
            }
        return{
            'discount_status': 'not created'
        }

    def delete(self):
        return Discount.delete_all()