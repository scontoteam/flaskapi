from flask_restful import Resource, reqparse, request
from flask_jwt import current_identity, JWT
from models.user import User, UserLike
from flask_jwt_extended import (
    create_access_token, create_refresh_token, jwt_required,
    jwt_refresh_token_required, get_jwt_identity,
    get_raw_jwt)



parser = reqparse.RequestParser()
parser.add_argument('email', help = 'Email cannot be blank', required = True)
parser.add_argument('password', help = 'Password cannot be blank', required = True)

class UserRegistration(Resource):
    def post(self):
        data = parser.parse_args()
        if User.find_by_email(data['email']):
            return {'message': 'User {} already exists'. format(data['email'])}
        new_user = User(
            email = data['email'],
            password = User.generate_hash(data['password'])
        )
        try:
            print('before saving to db')
            new_user.save_to_db()
            print('after saving to db')

            access_token = create_access_token(identity = new_user.id)
            refresh_token = create_refresh_token(identity = new_user.id)
            print('after token creating')
            return {
                'message': 'User {} was created'.format( data['email']),
                'access_token': access_token,
                'refresh_token': refresh_token
            }
        except:
            return {'message': 'Something went wrong'}, 500


class UserLogin(Resource):
    def post(self):
        data = parser.parse_args()
        current_user = User.find_by_email(data['email'])
        bad_credentials = { 'message': 'Pair with this email and password is incorrect'}
        if not current_user:
            return bad_credentials, 401
        if User.verify_hash(data['password'], current_user.password):
            access_token = create_access_token(identity = current_user.id)
            refresh_token = create_refresh_token(identity = current_user.id)
            return {
                'message': 'Logged in as {}'.format(current_user.email),
                'access_token': access_token,
                'refresh_token': refresh_token,
                'id': current_user.id
                }
        else:
            return bad_credentials, 401


class UserLogoutAccess(Resource):
    def post(self):
        return {'message': 'User logout'}
      
      
class UserLogoutRefresh(Resource):
    def post(self):
        return {'message': 'User logout'}
      
      
class TokenRefresh(Resource):
    @jwt_refresh_token_required
    def post(self):
        current_user = get_jwt_identity()
        access_token = create_access_token(identity = current_user)
        return {'access_token': access_token}
      
      
class AllUsers(Resource):
    def get(self):
        return User.return_all()

    # def delete(self):
    #     return User.delete_all()
class UserSingle(Resource):
    def delete(self,user_index):
        return User.delete_by_index(user_index)      
      
class SecretResource(Resource):
    @jwt_required
    def get(self):
        return {
            'answer': 42
        }

