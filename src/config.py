import os
class Config(object):
    """
    Base Configuration Class
    Contains all Application Constant
    Defaults
    """
    # Flask Settings
    DEBUG = False
    IS_PRODUCTION = False
    IS_STAGING = False

    db_host = 'remotemysql.com'
    db_user = os.environ.get("DB_USER") if os.environ.get("DB_USER") else 'kXNkNWjZ2q'
    db_pass = os.environ.get("DB_PASS") if os.environ.get("DB_PASS") else 'ZH7jMiCWjE'
    db_name = os.environ.get("DB_NAME") if os.environ.get("DB_NAME") else 'kXNkNWjZ2q'

    # Database Settings
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://{}:{}@{}/{}'.format(db_user, db_pass, db_host, db_name)
    SQLALCHEMY_POOL_RECYCLE = 500
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    SQLALCHEMY_POOL_SIZE = 10
    SQLALCHEMY_POOL_TIMEOUT = 10

    SQLALCHEMY_ECHO = True
    BASE_DIR = os.path.dirname(os.path.abspath(__file__))

    JWT_SECRET_KEY = 'djqwieji1j23p12endasldnj34123o[ejsaopjdopajepjrpj34ipj123ji2ejiadsjxipjeij12ip3j12cxcz1231123dsad'
    