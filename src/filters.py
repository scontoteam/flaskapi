from flask import request
from flask_restful import Resource
from models.discount import Discount, Category, TimeOfDay, Height, Overlay
from app import db

class CategoryResource(Resource):

    def get(self):
        category = Category.query.all()

        res = []
        for c in category:
            res.append(
                {
                    'id': c.id,
                    'name': c.name
                }
            )
        return res

    def post(self):
        payload = request.get_json(force=True)
        categories = Category(name=payload.get('name'))
        db.session.add(categories)
        db.session.commit()
        return {
            'success': True
        }

    def put(self, id):
        payload = request.get_json(force=True)
        cat_by_id = Category.query.filter(Category.id == id).first()
        cat_by_id.name = payload.get('name')
        db.session.commit()
        return{
            'updated': True
        }

    def delete(self, id):
        cat_by_id = Category.query.filter(Category.id==id).first()
        db.session.delete(cat_by_id)
        db.session.commit()
        return {
            'deleted': True
        }



class TimeResource(Resource):

    def get(self):
        time = TimeOfDay.query.all()

        res = []
        for t in time:
            res.append({
                "id": t.id,
                "name": t.name
            })

        return res

    def post(self):
        payload = request.get_json(force=True)
        times = TimeOfDay(name=payload.get('name'))
        db.session.add(times)
        db.session.commit()
        return {
            'success': True
        }


    def put(self, id):
        payload = request.get_json(force=True)
        time_by_id = TimeOfDay.query.filter(TimeOfDay.id == id).first()
        time_by_id.name = payload.get('name')
        db.session.commit()
        return{
            'updated': True
        }

    def delete(self, id):
        time_by_id = TimeOfDay.query.filter(TimeOfDay.id==id).first()
        db.session.delete(time_by_id)
        db.session.commit()
        return {
            'deleted': True
        }



class OverlayResource(Resource):

    def get(self):
        overlay = Overlay.query.all()

        res = []
        for o in overlay:
            res.append(
                {
                    'id': o.id,
                    'name': o.name
                }
            )
        return res

    def post(self):
        payload = request.get_json(force=True)
        overlays = Overlay(name=payload.get('name'))
        db.session.add(overlays)
        db.session.commit()
        return {
            'success': True
        }

    def put(self, id):
        payload = request.get_json(force=True)
        overlay_by_id = Overlay.query.filter(Overlay.id == id).first()
        overlay_by_id.name = payload.get('name')
        db.session.commit()
        return{
            'updated': True
        }

    def delete(self, id):
        overlay_by_id = Overlay.query.filter(Overlay.id==id).first()
        db.session.delete(overlay_by_id)
        db.session.commit()
        return {
            'deleted': True
        }



class HeightResource(Resource):

    def get(self):
        height = Height.query.all()

        res = []
        for h in height:
            res.append(
                {
                    'id': h.id,
                    'name': h.name
                }
            )
        return res

    def post(self):
        payload = request.get_json(force=True)
        heights = Height(name=payload.get('name'))
        db.session.add(heights)
        db.session.commit()
        return {
            'success': True
        }
        
    def put(self, id):
        payload = request.get_json(force=True)
        height_by_id = Height.query.filter(Height.id == id).first()
        height_by_id.name = payload.get('name')
        db.session.commit()
        return{
            'updated': True
        }

    def delete(self, id):
        height_by_id = Height.query.filter(Height.id==id).first()
        db.session.delete(height_by_id)
        db.session.commit()
        return {
            'deleted': True
        }
