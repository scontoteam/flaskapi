import werkzeug
from flask import Flask, render_template, request, flash, url_for, \
    send_from_directory, Response, jsonify
from flask_cors import CORS
from flask_restful import Resource, Api, reqparse
from werkzeug.utils import secure_filename, redirect
from flask_sqlalchemy import SQLAlchemy
from flask_session import Session
from flask_jwt_extended import JWTManager
import os
import datetime
from config import Config
app = Flask(__name__)
app.config.from_object(Config())
db = SQLAlchemy(app)
api = Api(app)
sess = Session()
app.config['SESSION_TYPE'] = 'memcached'
app.config['SECRET_KEY'] = 'redsfsfsfsfis'
app.config['JWT_ACCESS_TOKEN_EXPIRES'] = datetime.timedelta(days=3)
jwt = JWTManager(app)

# additional config settings
BASE_DIR = app.config['BASE_DIR']
CORS(app,resources={r"/api/*": {"origins": "*"}})



from filters import CategoryResource, TimeResource, OverlayResource, HeightResource
from models.discount import Discount
from urls.discount import *
import urls.user
import urls.likes


@app.route('/', methods=['GET','POST'])
def index():
    if request.method == 'POST':
        if 'file' not in request.files:
            return {'status':'file not founded'}

        file = request.files['file']
        if file.filename == '':
            return{'status':'file not selected'}

        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(UPLOAD_FOLDER, filename))
            return redirect(url_for('uploaded_file', filename=filename))

    return render_template('index.html')


@app.route('/uploads/<filename>', methods=['GET','POST'])
def uploaded_file(filename):
    return send_from_directory(UPLOAD_FOLDER, filename)


api.add_resource(CategoryResource, '/api/category/', '/api/category/<int:id>')
api.add_resource(TimeResource, '/api/time/', '/api/time/<int:id>')
api.add_resource(OverlayResource, '/api/overlay/', '/api/overlay/<int:id>')
api.add_resource(HeightResource, '/api/height/', '/api/height/<int:id>')

# urls
api.add_resource(DiscountsView, '/api/discounts/')
api.add_resource(DiscountSingleView, '/api/discounts/<discount_index>/')
api.add_resource(urls.user.UserRegistration, '/api/register/')
api.add_resource(urls.user.UserLogin, '/api/login/')
api.add_resource(urls.user.UserLogoutAccess, '/api/logout/access/')
api.add_resource(urls.user.UserLogoutRefresh, '/api/logout/refresh/')
api.add_resource(urls.user.TokenRefresh, '/api/token/refresh/')
api.add_resource(urls.user.AllUsers, '/api/users/')
api.add_resource(urls.user.UserSingle, '/api/users/<user_index>/')
api.add_resource(urls.user.SecretResource, '/api/secret/')
api.add_resource(urls.likes.UserLikes, '/api/likes/')
api.add_resource(urls.likes.UserLikeSingle, '/api/likes/<index>/')

api.add_resource(urls.likes.GetIdentity, '/api/identity/')

if __name__ == '__main__':
    app.secret_key = 'reajdiasjdjio2131dasd34131'
    app.run(debug=True)
